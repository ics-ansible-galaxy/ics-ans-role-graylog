import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('graylogs')


def test_daemon_running(host):
    assert host.socket("tcp://127.0.0.1:9200")  # elasticsearch api
    assert host.socket("tcp://127.0.0.1:9300")  # elasticsearch transport
    assert host.socket("tcp://127.0.0.1:9000")  # graylog web
    assert host.socket("tcp://127.0.0.1:27017")  # mongodb
    s = host.service("graylog-server")
    assert s.is_running
    assert s.is_enabled
