class FilterModule(object):
    def filters(self):
        return {"parse_graylog_inputs": parse_graylog_inputs}


def parse_graylog_inputs(g_inputs):
    parsed_data = {}
    assert isinstance(g_inputs, list)
    for entry in g_inputs:
        parsed_data[entry["title"]] = {
            "type": entry["type"],
            "global": entry["global"],
            "configuration": entry["attributes"],
        }
    return parsed_data


if __name__ == "__main__":
    inputs = [
        {
            "attributes": {
                "bind_address": "0.0.0.0",
                "decompress_size_limit": 8388608,
                "override_source": None,
                "port": 12201,
                "recv_buffer_size": 262144,
            },
            "content_pack": None,
            "created_at": "2018-10-09T12:25:39.542Z",
            "creator_user_id": "VALUE_SPECIFIED_IN_NO_LOG_PARAMETER",
            "global": True,
            "id": "5bbc9e4346e0fb2884cc681d",
            "name": "GELF UDP",
            "node": None,
            "static_fields": {},
            "title": "udp input",
            "type": "org.graylog2.inputs.gelf.udp.GELFUDPInput",
        }
    ]

    print(parse_graylog_inputs(inputs))
