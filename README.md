# ics-ans-role-graylog

Ansible role to install graylog.

## Requirements

- ansible >= 2.6
- molecule >= 2.6

## Role Variables

```yaml
# performance tuning: sum should be below the number of CPU
graylog_processbuffer: 8
graylog_outputbuffer: 3
graylog_inputbuffer: 2

graylog_email_enabled: true
graylog_email_hostname: smtp.domain.com
graylog_email_port: 25
graylog_email_use_auth: false
graylog_email_use_tls: false
graylog_email_use_ssl: false
graylog_email_from: account@domain.com
graylog_baseurl: "https://packages.graylog2.org/repo/el/stable/3.0/$basearch/"
graylog_version: 3.0.0-12
graylog_is_master: true
graylog_admin_user: admin
graylog_admin_password: admin
graylog_admin_mail: nobody@esss.se
graylog_admin_timezone: Europe/Stockholm
graylog_elasticsearch_hosts:
  - http://127.0.0.1:9200
graylog_mongodb_uris:
  - mongodb://localhost/graylog
graylog_bind_address: "{{ ansible_default_ipv4.address }}:9000"
graylog_http_publish_uri: "http://{{ graylog_bind_address }}"
graylog_inputs:
  - title: GELF UDP input
    type: org.graylog2.inputs.gelf.udp.GELFUDPInput
    global: true
    configuration:
      bind_address: 0.0.0.0
      port: 12201
  - title: GELF TCP input
    type: org.graylog2.inputs.gelf.tcp.GELFTCPInput
    global: true
    configuration:
      bind_address: 0.0.0.0
      port: 12201
  - title: Syslog UDP
    type: org.graylog2.inputs.syslog.udp.SyslogUDPInput
    global: true
    configuration:
      bind_address: 0.0.0.0
      port: 514
      allow_override_date: true
      expand_structured_data: false
      force_rdns: false
      recv_buffer_size: 262144
      store_full_message: false
      override_source: ""
  - title: Raw TCP
    type: org.graylog2.inputs.raw.tcp.RawTCPInput
    global: true
    configuration:
      allow_override_date: true
      bind_address: 0.0.0.0
      expand_structured_data: false
      force_rdns: false
      override_source": ""
      port: 9001
      recv_buffer_size: 262144
      store_full_message: false
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-graylog
```

## License

BSD 2-clause
